function swap(inventory,xp, yp)
{
    var temp = inventory[xp];
    inventory[xp] = inventory[yp];
    inventory[yp] = temp;
}
 
function alphabetical_sort(inventory)
{
    var i, j, min_idx;
 
    // One by one move boundary of unsorted subinventoryay
    for (i = 0; i < inventory.length; i++)
    {
        // Find the minimum element in unsorted inventoryay
        min_idx = i;
        for (j = i + 1; j < inventory.length; j++)
        if (inventory[j].car_model < inventory[min_idx].car_model)
            min_idx = j;
 
        // Swap the found minimum element with the first element
        swap(inventory,min_idx, i);
    }
    return inventory;
}

module.exports = alphabetical_sort;
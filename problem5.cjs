car_older_than_2000 = (inventory) => {
    car_year_list = []
    for (let i=0; i<inventory.length; i++) {
        if (inventory[i] < 2000) {
            car_year_list.push(inventory[i])
        }
    }
    return car_year_list;
}

module.exports = car_older_than_2000;
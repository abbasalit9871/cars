latest_add = (inventory) => {
    let car_info = inventory[0]; 
    let latest_year = inventory[0].car_year;
    for (let i=1; i<inventory.length; i++) {
        if (inventory[i].car_year >= latest_year) {
            latest_year = inventory[i].car_year;
            car_info = inventory[i]
        }
    }
    return car_info;
}

module.exports = latest_add;
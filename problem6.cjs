BMWandAudi = (inventory) => {
    bmw_or_audi = []
    for (let i=0; i<inventory.length; i++) {
        if (inventory[i].car_make == 'Audi' || inventory[i].car_make == 'BMW') {
            bmw_or_audi.push(inventory[i]);
        }
    }
    return bmw_or_audi;
}

module.exports = BMWandAudi;